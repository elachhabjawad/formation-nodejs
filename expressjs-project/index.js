const express = require('express')
const app = express()
const Joi = require('joi');
// Application use midlle json 
// transform  any bison to json
// body to json
app.use(express.json())

const courses = [
    { id: 1, title: 'Learn PHP' },
    { id: 2, title: 'Learn JS' }
]
app.get('/', (req, res) => {
    res.send('hello world');
})

app.get('/api/courses', (req, res) => {
    res.send(JSON.stringify(['Reactjs', 'Nodejs', 'Laravel', 'nodejs']));
})

app.get('/api/courses/:id', (req, res) => {
    // req.params
    // req.query
    //res.send(`Course ID: ${req.params.id}`);

    let id = req.params.id;


    const course = courses.find(course => course.id === parseInt(id));

    if (!course) {
        res.status(404).send('Course no found');
    } else {
        res.send(course)
    }
})


app.post('/api/courses', (req,res) => {

    const schema = Joi.object({
        title: Joi.string()
            .alphanum()
            .min(3)
            .max(30)
            .required(),
    });

    const { error, value } = schema.validate(req.body);

    if(error){
        res.status(400).send(error.details[0].message)
    }


    let course = {
        id: courses.length + 1,
        title: value.title
    }


    courses = [...courses, course];

    res.send(course)
})



const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`The app listening the post ${port}`);
})
