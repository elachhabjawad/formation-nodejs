// function add(x, y) {
//     console.log(`${x} + ${y} = ${x + y}`);
// }

// function sub(x, y) {
//     console.log(`${x} - ${y} = ${x + y}`);
// }

// function mult(x, y) {
//     console.log(`${x} * ${y} = ${x * y}`);
// }

// function div(x, y) {
//     console.log(`${x} / ${y} = ${x / y}`);
// }


exports.add = (x, y) => console.log(`${x} + ${y} = ${x + y}`)

exports.sub = (x, y) => console.log(`${x} - ${y} = ${x + y}`);

exports.mult = (x, y) => console.log(`${x} * ${y} = ${x * y}`);

exports.div = (x, y) => console.log(`${x} / ${y} = ${x / y}`);

// Cas 1
// module.exports.add = add;
// module.exports.sub = sub;
// module.exports.mult = mult;
// module.exports.div = div;

// Cas 2
// exports.add = add;
// exports.sub = sub;
// exports.mult = mult;
// exports.div = div;