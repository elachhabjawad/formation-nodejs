const calc = require('./calc');
const path = require('path');
const myOs = require('os');
const fs = require('fs');
const EventEmitter = require('events');
// calc.add(5,5)
// calc.sub(5, 5)
// calc.mult(5, 5)
// calc.div(5, 5)

// Use module Path
console.log(__dirname);
console.log(__filename);
console.log(path.parse(__filename)); // Use module Path
// Use module OS
//console.log(myOs.cpus())
let totalMemory = myOs.totalmem();
let freeMemory = myOs.freemem();
console.log(`Total memory is : ${totalMemory} - Free Memory : ${freeMemory}`);

// Use module FS
// using le mode sync
// const files = fs.readdirSync('./');
// console.log(files);
// console.log('test');

// using le mode Async
// fs.readdir('./',(err,files)=>{
//      if (err){
//         console.error(error)
//     }else{
//         console.table(files);
//     }
// })

// Use module events
const myEmitter = new EventEmitter();

// sans parmas
// myEmitter.on('hello', () => {
//     console.log(`Hello`);
// })
// myEmitter.emit('hello');

// with params and send tow params
// myEmitter.on('hello',(name,phone)=>{
//     console.log(`Hello my name is ${name} and phone is ${phone}`);
// })
// myEmitter.emit('hello','Jawad','0655669922');

// with params and send object
myEmitter.on('hello', (event) => {
    console.log(`Hello my name is ${event.name} and phone is ${event.phone}`);
})
myEmitter.emit('hello', { name: 'Jawad', phone: '0655669922' });