const http = require('http');
// create server
const server = http.createServer((req,res)=>{
    if(req.url==='/'){
        res.write('hello I\'m Jawad' );
        res.end();
    }

    else if (req.url === '/api/courses'){
        res.write(JSON.stringify(['Reactjs','Nodejs']));
        res.end();
    }
    
});

server.on('connection',()=>{
    console.log('new connection');
})

console.log('listening on port 4000');
// stting port
server.listen(4000);